#define NULL 0

// funkcja 1
void CopyString(char pcSource[], char pcDestination[]){
	
	unsigned char ucCharCounter;
	
	for(ucCharCounter = 0; pcSource[ucCharCounter] != NULL; ucCharCounter++){
		pcDestination[ucCharCounter] = pcSource[ucCharCounter];
	}
	pcDestination[ucCharCounter] = NULL;
}

// funkcja 2
enum CompResult{ NOT_EQUAL, EQUAL};

enum CompResult eCompareString(char pcStr1[], char pcStr2[]){

	unsigned char ucCharCounter;
	
	for(ucCharCounter = 0; pcStr1[ucCharCounter] != NULL && pcStr2[ucCharCounter] != NULL; ucCharCounter++){
		if(pcStr1[ucCharCounter] != pcStr2[ucCharCounter]){
			return NOT_EQUAL;
		}
	}
	return EQUAL;
}

// funkcja 3
void AppendString(char pcSourceStr[], char pcDestinationStr[]){
	
	unsigned char ucCharCounter;
	
	for(ucCharCounter = 0; pcDestinationStr[ucCharCounter] != NULL; ucCharCounter++) {}
		CopyString(pcSourceStr, pcDestinationStr + ucCharCounter);
	}

// funkcja 4
void ReplaceCharactersInString(char pcString[],char cOldChar,char cNewChar){
    unsigned char iCharCounter;

    for (iCharCounter = 0 ; pcString[iCharCounter] != NULL ; iCharCounter++){
        if (pcString[iCharCounter] == cOldChar) {
            pcString[iCharCounter] = cNewChar;
        }
    }
}

// funkcja 5

#define BITMASK 0x0000000F

void UIntToHexStr(unsigned int uiValue, char pcStr[]) {
	
	unsigned char ucNibbleCounter;
	unsigned char ucCurrentNibble;
	
	pcStr[0] = '0';
	pcStr[1] = 'x';
	pcStr[10] = NULL;
	
	for(ucNibbleCounter = 0; ucNibbleCounter < 8; ucNibbleCounter++){
		ucCurrentNibble = (uiValue >> (ucNibbleCounter * 4)) & BITMASK;
		if(ucCurrentNibble < 10){
			pcStr[9 - ucNibbleCounter] = '0' + ucCurrentNibble;
		}
		else{
			pcStr[9 - ucNibbleCounter] = 'A' + (ucCurrentNibble - 10);
		}
	}
}

// funkcja 6

enum Result {OK, ERROR};

enum Result eHexStringToUInt(char pcStr[],unsigned int *puiValue){

	unsigned char ucCharCounter;
	unsigned char ucCurrentChar;
	
	*puiValue = 0;

	if((pcStr[0] != '0') || (pcStr[1] != 'x') || (pcStr[2] == NULL)){
		return ERROR;
	}
	for(ucCharCounter=2; ucCharCounter != NULL; ucCharCounter++){
		ucCurrentChar = pcStr[ucCharCounter];
		if(ucCurrentChar == NULL){
			return OK;
		}
		else if(ucCharCounter == 6){
			return ERROR;
		}
		*puiValue = *puiValue << 4;
		if((ucCurrentChar <= '9') && (ucCurrentChar >= '0')){
			ucCurrentChar = ucCurrentChar - '0';
		}
		else if((ucCurrentChar <= 'F') && (ucCurrentChar >= 'A')){
			ucCurrentChar = ucCurrentChar - 'A' + 10;
		}
		else{
			return ERROR;
		}
		*puiValue = (*puiValue) | ucCurrentChar;
	}
	return ERROR;
}

//funkcja 7

void AppendUIntToString (unsigned int uiValue, char pcDestinationStr[]){
	
	unsigned char ucCharCounter;
	
	for(ucCharCounter = 0; pcDestinationStr[ucCharCounter] != NULL; ucCharCounter++) {}
		UIntToHexStr(uiValue, &pcDestinationStr[ucCharCounter]);
}
