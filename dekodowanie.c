#include "string.h"

#define MAX_KEYWORD_STRING_LTH 10 // maks dlugosc lancuchu
#define MAX_KEYWORD_NR 3 // maksymalna liczba keywordow
#define MAX_TOKEN_NR 3 // liczba keywordow

#define NULL 0

enum KeywordCode {LD, ST, RST} eKeywordCode; // definicja slow kluczowych

struct Keyword // definicja strucktury keyword
{
	enum KeywordCode eCode; // kod slowa kluczowego
	char cString[MAX_KEYWORD_STRING_LTH + 1]; // powiazany z nim lancuch znakowy
};

union TokenValue // deklaracja unii zmiennych - wartosc tokenu
{
	enum KeywordCode eKeyword; // keyword/liczba/wskaznik na lancuch znakowy/
	unsigned int uiNumber;
	char * pcString;
};


enum TokenType {KEYWORD, NUMBER, STRING}; // definicja typu tokenu

struct Token{ // deklaracja struktury tokenu: 
	enum TokenType eType; // typ tokenu
	union TokenValue uValue; // wartosc tokenu
};

struct Token asToken[MAX_TOKEN_NR]; // definicja tablicy tokenow

struct Keyword asKeywordList[MAX_KEYWORD_NR]= // struktura keyword:
{ // kod slowa kluczowego i powiazanaego z nim lancucha znakowego
{RST,"reset"},
{LD, "load" },
{ST, "store"}
};

unsigned char ucTokenNr;

// funkcja 1
////Co wchodzi:
// lancuch znakowy
//// Co wychodzi:
// zwraca wskazniki na poczatki znalezionych tokenow
// liczbe Tokenow
// wpisuje je do tablicy asToken w polu uValue jako pcString
//// Po czym iteruje:
// po znakach w lanuchu
//// Co robi:
//
//
// zwraca wskazniki na poczatki znalezionych tokenow
// 

unsigned char ucFindTokensInString (char *pcString){
	
	unsigned char ucCharCounter;
	unsigned char ucCurrentChar;

	enum State {TOKEN, DELIMITER} eState = DELIMITER;

	ucTokenNr = 0;

	for(ucCharCounter = 0; ;ucCharCounter++){
		ucCurrentChar = pcString[ucCharCounter];
		
		switch(eState){
			case TOKEN:
				if(ucTokenNr == MAX_TOKEN_NR){
					return ucTokenNr;
				}
				else if(ucCurrentChar == NULL){
					return ucTokenNr;
				}
				else if(ucCurrentChar != ' '){
					eState = TOKEN;
				}
				else{
					eState = DELIMITER;
				}
				break;
			case DELIMITER:
				if(ucCurrentChar == NULL){
					return ucTokenNr;
				}
				else if(ucCurrentChar == ' '){
					eState = DELIMITER;
				}
				else{
					asToken[ucTokenNr].uValue.pcString = pcString + ucCharCounter;
					ucTokenNr++;
					eState = TOKEN;
				}
				break;
		}
	}
}


// funkcja 2
//// Co wchodzi:
// lanuch znakowy, wskaznik na strukture keywordCode 
//// Co wychodzi:
// zmodyfikowany lanucuch znakowy na kod slowa kluczowego
//// Co robi:
// modyfikje lancuch znakowy na kod slowa kluczowego jesli zwroci EQUAL 
// porownuje lanuch znakowy ze powizannym z slowem kluczowym lanuchem w strukturze keyword - CompareString
//// Po czym iteruje:
// po tablicy Keywordow
// zwraca OK/ERROR jesli udalo sie zamienic lanuch znakowy w na kod slowa kluczowego



enum Result{OK,ERROR} eResult;

enum Result eStringToKeyword(char pcStr[], enum KeywordCode *peKeywordCode){
	
	unsigned char ucKeywordCounter;
	
	for(ucKeywordCounter = 0; ucKeywordCounter < MAX_KEYWORD_NR; ucKeywordCounter++){
		if(EQUAL == eCompareString(asKeywordList[ucKeywordCounter].cString, pcStr)){
			*peKeywordCode = asKeywordList[ucKeywordCounter].eCode;
			return OK;
		}
	}
	return ERROR;
}


// funkcja 3
//// Co wchodzi:
//  
//// Co wychodzi:
// 
//// Co robi:
// okresla typ i wartosc tokenui wpisuje do tablicy asToken
// sprawdza czy number jak nie to sprawdza czy keyword jak nie to ustala jako string
//// Po czym iteruje:
// po tokenach(tablica asToken)

void DecodeTokens(void){
	
	unsigned char ucTokenCounter;
	struct Token *psCurrentToken;
	unsigned int uiTokenValue;
	enum KeywordCode eTokenCode;
	
	for(ucTokenCounter = 0; ucTokenCounter < ucTokenNr; ucTokenCounter++){
		psCurrentToken = &asToken[ucTokenCounter];
		
		if(OK == eHexStringToUInt(psCurrentToken->uValue.pcString, &uiTokenValue)){
			psCurrentToken->eType = NUMBER;
			psCurrentToken->uValue.uiNumber = uiTokenValue;
		}
		else if(OK == eStringToKeyword(psCurrentToken->uValue.pcString, &eTokenCode)){
			psCurrentToken->eType = KEYWORD;
			psCurrentToken->uValue.eKeyword = eTokenCode;
		}
		else{
			psCurrentToken->eType = STRING;
		}
	}
}

// funkcja 4
//// Co wchodzi:
// wskaznik na lanuch znakowy
//// Co wychodzi:
// zdekodowany komunikat
//// Co robi:
// scala poprzednie funkcje
// wypelnia tablice asToken i ustawia zmienna ucTokenNr
// indeksuje poczatki tokenow, zamienia delimetery na Nulle,
// dekoduje poszczegolnie Tokeny
//// Po czym iteruje:
// --------------------

void DecodeMsg(char *pcString){
	
	ucTokenNr = ucFindTokensInString(pcString);
	ReplaceCharactersInString(pcString, ' ', NULL);
	DecodeTokens();
}

// testy!!!

char pcString[] = "reset 0x234 something";

int main(){
//	unsigned int TokenNr = ucFindTokensInString("reset 0x234 something");

//	eResult = eStringToKeyword("reset 0x234 something", &eKeywordCode);
//	
//	DecodeTokens();

	DecodeMsg(pcString);
}
